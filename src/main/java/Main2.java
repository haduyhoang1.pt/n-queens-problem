import java.util.*;
import java.util.stream.Collectors;

public class Main2 {

	public static final double C1 = 0.45;
	public static final long C2 = 34l;
//	public static final long C2 = 17l;
	public static final Random random = new Random();

	/**
	 * @param queen is empty
	 * @param n     is the number of queens = size of chessboard
	 */
	public static void initializeQueenRandom(List<Integer> queen, int n) {
		for (int i = 0; i < n; i++) {
			queen.add(i);
		}
		Collections.shuffle(queen, new Random());
	}

	/**
	 * @param row
	 * @param column
	 * @return index of diagonal line negative
	 */
	public static int computeIndexDiagonalLinesNegative(int row, int column) {
		return row + column;
	}

	/**
	 * @param row
	 * @param column
	 * @param sizeOfBoard is the number of queens = size of chessboard
	 * @return index of diagonal line positive
	 */
	public static int computeIndexDiagonalLinesPositive(int row, int column, int sizeOfBoard) {
		return row - column + (sizeOfBoard - 1);
	}

	/**
	 * @param queenOnDiagonal
	 * @return collision by quantity queen on diagonal
	 */
	public static int getCollisionOnDiagonal(int queenOnDiagonal) {
		return queenOnDiagonal > 0 ? queenOnDiagonal - 1 : 0;
	}

	/**
	 * @param queen contains the position of the queen
	 * @param qn    is empty. Initializes array qn - store count queen on diagonal negative
	 * @param qp    is empty. Initializes array qp - store count queen on diagonal positive
	 * @return total number of collisions on the diagonal lines that exist in the initial queen placement
	 */
	public static int countQueenOnDiagonal(List<Integer> queen, List<Integer> qn, List<Integer> qp) {
		int totalCollisions = 0;
		int numberOfDiagonals = 2 * queen.size() - 1;

		// initialize
		for (int i = 0; i < numberOfDiagonals; i++) {
			qn.add(0);
			qp.add(0);
		}

		// count queen on diagonal lines
		for (int i = 0; i < queen.size(); i++) {
			// diagonal lines negative
			Integer indexDiagonalLinesNegative = computeIndexDiagonalLinesNegative(i, queen.get(i));
			qn.set(indexDiagonalLinesNegative, qn.get(indexDiagonalLinesNegative) + 1);

			// diagonal lines positive
			Integer indexDiagonalLinesPositive = computeIndexDiagonalLinesPositive(i, queen.get(i), queen.size());
			qp.set(indexDiagonalLinesPositive, qp.get(indexDiagonalLinesPositive) + 1);
		}

		// collisions = number of queen on diagonal line - 1
		for (int i = 0; i < numberOfDiagonals; i++) {
			Integer collisionDiagonalLinesNegative = getCollisionOnDiagonal(qn.get(i));
			Integer collisionDiagonalLinesPositive = getCollisionOnDiagonal(qp.get(i));
			totalCollisions += collisionDiagonalLinesNegative + collisionDiagonalLinesPositive;
		}

		return totalCollisions;
	}

	/**
	 * @param queen
	 * @param attack is empty. Initializes array attack contain index of array queen is attacks
	 * @param qn
	 * @param qp
	 * @return the number of attacked queen
	 */
	public static int computeAttacks(List<Integer> queen, List<Integer> attack, List<Integer> qn, List<Integer> qp) {
		for (int i = 0; i < queen.size(); i++) {
			int indexDiagonalLinesNegative = computeIndexDiagonalLinesNegative(i, queen.get(i));
			int indexDiagonalLinesPositive = computeIndexDiagonalLinesPositive(i, queen.get(i), queen.size());
			if (qn.get(indexDiagonalLinesNegative) > 2 || qp.get(indexDiagonalLinesPositive) > 2) {
				attack.add(i);
			}
		}
		return attack.size();
	}

	// return new collision if swap
	public static int computeCollisionIfSwapTemp(List<Integer> queen, List<Integer> qn, List<Integer> qp, Integer indexRow1, Integer indexRow2) {
		Map<Integer, Integer> tempQn = new HashMap<>();
		Map<Integer, Integer> tempQp = new HashMap<>();
		int oldCollision;
		int newCollision;

//		List<Integer> queenTemp = new ArrayList<>(queen);
//		List<Integer> qnTemp = new ArrayList<>(qn);
//		List<Integer> qpTemp = new ArrayList<>(qp);
//		Integer collisionTemp1 = countQueenOnDiagonal(queenTemp, new ArrayList<>(), new ArrayList<>());
//		swapQueen(queenTemp, qnTemp, qpTemp, indexRow1, indexRow2);
//		Integer collisionTemp2 = countQueenOnDiagonal(queenTemp, new ArrayList<>(), new ArrayList<>());
//		Integer temp = collisionTemp2 - collisionTemp1;

		// save old value qn, qp of old position queen 1 on the chessboard
		Integer indexDiagonalLinesNegative1 = computeIndexDiagonalLinesNegative(indexRow1, queen.get(indexRow1));
		Integer indexDiagonalLinesPositive1 = computeIndexDiagonalLinesPositive(indexRow1, queen.get(indexRow1), queen.size());
		tempQn.put(indexDiagonalLinesNegative1, qn.get(indexDiagonalLinesNegative1));
		tempQp.put(indexDiagonalLinesPositive1, qp.get(indexDiagonalLinesPositive1));

		// save old value qn, qp of old position queen 2 on the chessboard
		Integer indexDiagonalLinesNegative2 = computeIndexDiagonalLinesNegative(indexRow2, queen.get(indexRow2));
		Integer indexDiagonalLinesPositive2 = computeIndexDiagonalLinesPositive(indexRow2, queen.get(indexRow2), queen.size());
		tempQn.put(indexDiagonalLinesNegative2, qn.get(indexDiagonalLinesNegative2));
		tempQp.put(indexDiagonalLinesPositive2, qp.get(indexDiagonalLinesPositive2));

		// save old value qn, qp of new column queen 1 on the chessboard
		Integer indexDiagonalLinesNegativeNew1 = computeIndexDiagonalLinesNegative(indexRow1, queen.get(indexRow2));
		Integer indexDiagonalLinesPositiveNew1 = computeIndexDiagonalLinesPositive(indexRow1, queen.get(indexRow2), queen.size());
		tempQn.put(indexDiagonalLinesNegativeNew1, qn.get(indexDiagonalLinesNegativeNew1));
		tempQp.put(indexDiagonalLinesPositiveNew1, qp.get(indexDiagonalLinesPositiveNew1));

		// save old value qn, qp of new column queen 2 on the chessboard
		Integer indexDiagonalLinesNegativeNew2 = computeIndexDiagonalLinesNegative(indexRow2, queen.get(indexRow1));
		Integer indexDiagonalLinesPositiveNew2 = computeIndexDiagonalLinesPositive(indexRow2, queen.get(indexRow1), queen.size());
		tempQn.put(indexDiagonalLinesNegativeNew2, qn.get(indexDiagonalLinesNegativeNew2));
		tempQp.put(indexDiagonalLinesPositiveNew2, qp.get(indexDiagonalLinesPositiveNew2));

		oldCollision = tempQn.values().stream().collect(Collectors.summingInt(v -> getCollisionOnDiagonal(v))) +
				tempQp.values().stream().collect(Collectors.summingInt(v -> getCollisionOnDiagonal(v)));

		// calculate qn when put up queen
		tempQn.replace(indexDiagonalLinesNegative1, tempQn.get(indexDiagonalLinesNegative1) - 1);
		tempQn.replace(indexDiagonalLinesNegative2, tempQn.get(indexDiagonalLinesNegative2) - 1);

		// calculate qn when put in queen
		tempQn.replace(indexDiagonalLinesNegativeNew1, tempQn.get(indexDiagonalLinesNegativeNew1) + 1);
		tempQn.replace(indexDiagonalLinesNegativeNew2, tempQn.get(indexDiagonalLinesNegativeNew2) + 1);

		// calculate qp when put up queen
		tempQp.replace(indexDiagonalLinesPositive1, tempQp.get(indexDiagonalLinesPositive1) - 1);
		tempQp.replace(indexDiagonalLinesPositive2, tempQp.get(indexDiagonalLinesPositive2) - 1);

		// calculate qp when put in queen
		tempQp.replace(indexDiagonalLinesPositiveNew1, tempQp.get(indexDiagonalLinesPositiveNew1) + 1);
		tempQp.replace(indexDiagonalLinesPositiveNew2, tempQp.get(indexDiagonalLinesPositiveNew2) + 1);

		newCollision = tempQn.values().stream().mapToInt(Main2::getCollisionOnDiagonal).sum() + tempQp.values().stream().mapToInt(Main2::getCollisionOnDiagonal).sum();

//		if (temp != newCollision - oldCollision) {
//			System.out.println("error");
//		}
		return newCollision - oldCollision;
	}

	public static void swapQueen(List<Integer> queen, List<Integer> qn, List<Integer> qp, Integer indexRow1, Integer indexRow2) {
		// queen 1 old column
		int indexDiagonalLinesNegative1 = computeIndexDiagonalLinesNegative(indexRow1, queen.get(indexRow1));
		int indexDiagonalLinesPositive1 = computeIndexDiagonalLinesPositive(indexRow1, queen.get(indexRow1), queen.size());
		qn.set(indexDiagonalLinesNegative1, qn.get(indexDiagonalLinesNegative1) - 1);
		qp.set(indexDiagonalLinesPositive1, qp.get(indexDiagonalLinesPositive1) - 1);

		// queen 2 old column
		int indexDiagonalLinesNegative2 = computeIndexDiagonalLinesNegative(indexRow2, queen.get(indexRow2));
		int indexDiagonalLinesPositive2 = computeIndexDiagonalLinesPositive(indexRow2, queen.get(indexRow2), queen.size());
		qn.set(indexDiagonalLinesNegative2, qn.get(indexDiagonalLinesNegative2) - 1);
		qp.set(indexDiagonalLinesPositive2, qp.get(indexDiagonalLinesPositive2) - 1);

		Integer indexColumnTemp = queen.get(indexRow1);
		queen.set(indexRow1, queen.get(indexRow2));
		queen.set(indexRow2, indexColumnTemp);

		// queen 1 new column
		int indexDiagonalLinesNegativeNew1 = computeIndexDiagonalLinesNegative(indexRow1, queen.get(indexRow1));
		int indexDiagonalLinesPositiveNew1 = computeIndexDiagonalLinesPositive(indexRow1, queen.get(indexRow1), queen.size());
		qn.set(indexDiagonalLinesNegativeNew1, qn.get(indexDiagonalLinesNegativeNew1) + 1);
		qp.set(indexDiagonalLinesPositiveNew1, qp.get(indexDiagonalLinesPositiveNew1) + 1);

		// queen 2 new column
		int indexDiagonalLinesNegativeNew2 = computeIndexDiagonalLinesNegative(indexRow2, queen.get(indexRow2));
		int indexDiagonalLinesPositiveNew2 = computeIndexDiagonalLinesPositive(indexRow2, queen.get(indexRow2), queen.size());
		qn.set(indexDiagonalLinesNegativeNew2, qn.get(indexDiagonalLinesNegativeNew2) + 1);
		qp.set(indexDiagonalLinesPositiveNew2, qp.get(indexDiagonalLinesPositiveNew2) + 1);
	}

	public static List<Integer> searchQueen(int n) {
		int collision = 1;                    // stores total number of collisions on diagonal line
		List<Integer> queen = null;           // stored column positions
		List<Integer> attack;                // stores row indexes of queens in array queen that are attacked by other queen
		List<Integer> qn;                    // stores number of queen on diagonal lines with negative slope
		List<Integer> qp;                    // stores number of queen on diagonal lines with positive slope

		while (collision > 0) {
			queen = new ArrayList<>();
			attack = new ArrayList<>();
			qn = new ArrayList<>();
			qp = new ArrayList<>();

			initializeQueenRandom(queen, n);
			collision = countQueenOnDiagonal(queen, qn, qp);
			computeAttacks(queen, attack, qn, qp);

			System.out.println("======================================");
//			System.out.println(queen);

			double limit = C1 * collision;
			long maxLoop = C2 * queen.size() * queen.size();
			long loopCount = 0l;

			while (loopCount <= maxLoop) {
				for (int i = 0; i < attack.size(); i++) {
					Integer indexRow = attack.get(i);
					int indexRowRandom;
					do {
						indexRowRandom = random.nextInt(queen.size() - 1);
					} while (indexRowRandom == indexRow);

					int differenceCollision = computeCollisionIfSwapTemp(queen, qn, qp, indexRow, indexRowRandom);
					if (differenceCollision < 0) {
//						System.out.print("index row: " + indexRow + ", ");
//						System.out.print("index row random: " + indexRowRandom + ", ");

						swapQueen(queen, qn, qp, indexRow, indexRowRandom);
						collision += differenceCollision;
						System.out.println("collision: " + collision);
						if (collision == 0) {
							System.out.println("=========== loop count: " + loopCount + " ===========");
							return queen;
						}
						if (collision < limit) {
							limit = C1 * collision;
							computeAttacks(queen, attack, qn, qp);
							i = 0;
						}
					}
				}
				loopCount += attack.size();
			}
		}

		collision = countQueenOnDiagonal(queen, new ArrayList<>(), new ArrayList<>());
		System.out.println("=========== collision: " + collision + " ===========");
		return queen;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter N: ");
		int n = sc.nextInt();

		long range = System.currentTimeMillis();
		List<Integer> queen = searchQueen(n);
		range = System.currentTimeMillis() - range;
		System.out.println("======================= time: " + range + " =======================");

		System.out.println("======================= check =======================");
		check(queen);
		System.out.println("======================= check done =======================");
	}

	public static void check(List<Integer> queen) {
		List<Integer> qn = new ArrayList<>();
		List<Integer> qp = new ArrayList<>();
		// initialize
		int numberOfDiagonals = 2 * queen.size() - 1;
		for (int i = 0; i < numberOfDiagonals; i++) {
			qn.add(0);
			qp.add(0);
		}

		// check trùng trên chỉ số cột
		// queen phải không chứa các phần tử trùng nhau
		if (queen.stream().distinct().count() != queen.size()) {
			System.out.println("array queen having tow element same value (column positions)");
			return;
		}
		// queen không chứa các phần tử trùng nhau và phần tử lớn nhất là n = size của mảng => queen là mảng hoán vị của 0 -> n-1
		Integer maxIndexColumn = queen.stream().distinct().max(Integer::compareTo).get();
		if (maxIndexColumn != queen.size() - 1) {
			System.out.println("array Q is not continuous from 1 to N. max = " + maxIndexColumn);
			return;
		}

		// check trùng trên đường chéo
		for (int i = 0; i < queen.size(); i++) {
			// diagonal lines negative
			int indexDiagonalLinesNegative = computeIndexDiagonalLinesNegative(i, queen.get(i));
			if (qn.set(indexDiagonalLinesNegative, qn.get(indexDiagonalLinesNegative) + 1) > 0) {
				System.out.printf("diagonal line negative have index: %d wrong%n", indexDiagonalLinesNegative);
				return;
			}

			// diagonal lines positive
			int indexDiagonalLinesPositive = computeIndexDiagonalLinesPositive(i, queen.get(i), queen.size());
			if (qp.set(indexDiagonalLinesPositive, qp.get(indexDiagonalLinesPositive) + 1) > 0) {
				System.out.printf("diagonal line positive have index: %d wrong%n", indexDiagonalLinesPositive);
				return;
			}
		}

	}

}
