package oop;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.*;

@Log4j2
public class ChessBoard {
    private static final Random random = new Random();

    private final double C1 = 0.45;
    private final long C2 = 34l;
    private final int size;
    private int numberOfDiagonals;

    // index là chỉ số hàng, value của index là chỉ số cột
    @Getter
    private int[] coordinatesChessBoard;

    private int[] queenOnNegativeLine;
    private int[] queenOnPositiveLine;

    List<Integer> attack = new ArrayList<>();

    public ChessBoard(int size) {
        this.size = size;
        this.numberOfDiagonals = 2 * this.size - 1;
        this.coordinatesChessBoard = new int[size];
        this.queenOnNegativeLine = new int[numberOfDiagonals];
        this.queenOnPositiveLine = new int[numberOfDiagonals];
        initializeCoordinatesChessBoard();
    }

    private void initializeCoordinatesChessBoard() {
        for (int i = 0; i < this.size; i++) {
            coordinatesChessBoard[i] = i;
        }
    }

    public void shuffleArray() {
        for (int i = coordinatesChessBoard.length - 1; i > 0; i--)
        {
            int index = random.nextInt(i + 1);
            int a = coordinatesChessBoard[index];
            coordinatesChessBoard[index] = coordinatesChessBoard[i];
            coordinatesChessBoard[i] = a;
        }
    }

    /**
     * @param row
     * @param column
     * @return index of diagonal line negative
     */
    public int computeIndexDiagonalLinesNegative(int row, int column) {
        return row + column;
    }

    /**
     * @param row
     * @param column
     * @return index of diagonal line positive
     */
    public int computeIndexDiagonalLinesPositive(int row, int column) {
        return row - column + (size - 1);
    }

    /**
     * @param queenOnDiagonal
     * @return collision by quantity queen on diagonal
     */
    private int getCollisionOnDiagonal(int queenOnDiagonal) {
        return queenOnDiagonal > 0 ? queenOnDiagonal - 1 : 0;
    }

    /**
     * @return total number of collisions on the diagonal lines that exist in the initial queen placement
     */
    public int countQueenOnDiagonal() {
        int totalCollisions = 0;

        // initialize
        for (int i = 0; i < numberOfDiagonals; i++) {
            queenOnNegativeLine[i] = 0;
            queenOnPositiveLine[i] = 0;
        }

        // count queen on diagonal lines
        for (int i = 0; i < this.size; i++) {
            // diagonal lines negative
            int indexDiagonalLinesNegative = computeIndexDiagonalLinesNegative(i, this.coordinatesChessBoard[i]);
            queenOnNegativeLine[indexDiagonalLinesNegative] = queenOnNegativeLine[indexDiagonalLinesNegative] + 1;

            // diagonal lines positive
            int indexDiagonalLinesPositive = computeIndexDiagonalLinesPositive(i, this.coordinatesChessBoard[i]);
            queenOnPositiveLine[indexDiagonalLinesPositive] = queenOnPositiveLine[indexDiagonalLinesPositive] + 1;
        }

        // collisions = number of queen on diagonal line - 1
        for (int i = 0; i < numberOfDiagonals; i++) {
            int collisionDiagonalLinesNegative = getCollisionOnDiagonal(queenOnNegativeLine[i]);
            int collisionDiagonalLinesPositive = getCollisionOnDiagonal(queenOnPositiveLine[i]);
            totalCollisions += collisionDiagonalLinesNegative + collisionDiagonalLinesPositive;
        }

        return totalCollisions;
    }

    /**
     * @return the number of attacked queen
     */
    public void computeAttacks() {
        for (int i = 0; i < size; i++) {
            int indexDiagonalLinesNegative = computeIndexDiagonalLinesNegative(i, coordinatesChessBoard[i]);
            int indexDiagonalLinesPositive = computeIndexDiagonalLinesPositive(i, coordinatesChessBoard[i]);
            if (queenOnNegativeLine[indexDiagonalLinesNegative] > 2 || queenOnPositiveLine[indexDiagonalLinesPositive] > 2) {
                this.attack.add(i);
            }
        }
    }

    // return new collision if swap
    public int computeCollisionIfSwapTemp(int indexRow1, int indexRow2) {
        Map<Integer, Integer> tempQn = new HashMap<>();
        Map<Integer, Integer> tempQp = new HashMap<>();
        int oldCollision;
        int newCollision;

        // save old value qn, qp of old position queen 1 on the chessboard
        int indexDiagonalLinesNegative1 = computeIndexDiagonalLinesNegative(indexRow1, this.coordinatesChessBoard[indexRow1]);
        int indexDiagonalLinesPositive1 = computeIndexDiagonalLinesPositive(indexRow1, this.coordinatesChessBoard[indexRow1]);
        tempQn.put(indexDiagonalLinesNegative1, queenOnNegativeLine[indexDiagonalLinesNegative1]);
        tempQp.put(indexDiagonalLinesPositive1, queenOnPositiveLine[indexDiagonalLinesPositive1]);

        // save old value qn, qp of old position queen 2 on the chessboard
        int indexDiagonalLinesNegative2 = computeIndexDiagonalLinesNegative(indexRow2, this.coordinatesChessBoard[indexRow2]);
        int indexDiagonalLinesPositive2 = computeIndexDiagonalLinesPositive(indexRow2, this.coordinatesChessBoard[indexRow2]);
        tempQn.put(indexDiagonalLinesNegative2, queenOnNegativeLine[indexDiagonalLinesNegative2]);
        tempQp.put(indexDiagonalLinesPositive2, queenOnPositiveLine[indexDiagonalLinesPositive2]);

        // save old value qn, qp of new column queen 1 on the chessboard
        int indexDiagonalLinesNegativeNew1 = computeIndexDiagonalLinesNegative(indexRow1, this.coordinatesChessBoard[indexRow2]);
        int indexDiagonalLinesPositiveNew1 = computeIndexDiagonalLinesPositive(indexRow1, this.coordinatesChessBoard[indexRow2]);
        tempQn.put(indexDiagonalLinesNegativeNew1, queenOnNegativeLine[indexDiagonalLinesNegativeNew1]);
        tempQp.put(indexDiagonalLinesPositiveNew1, queenOnPositiveLine[indexDiagonalLinesPositiveNew1]);

        // save old value qn, qp of new column queen 2 on the chessboard
        int indexDiagonalLinesNegativeNew2 = computeIndexDiagonalLinesNegative(indexRow2, this.coordinatesChessBoard[indexRow1]);
        int indexDiagonalLinesPositiveNew2 = computeIndexDiagonalLinesPositive(indexRow2, this.coordinatesChessBoard[indexRow1]);
        tempQn.put(indexDiagonalLinesNegativeNew2, queenOnNegativeLine[indexDiagonalLinesNegativeNew2]);
        tempQp.put(indexDiagonalLinesPositiveNew2, queenOnPositiveLine[indexDiagonalLinesPositiveNew2]);

        oldCollision = tempQn.values().stream().mapToInt(this::getCollisionOnDiagonal).sum() + tempQp.values().stream().mapToInt(this::getCollisionOnDiagonal).sum();

        // calculate qn when put up queen
        tempQn.replace(indexDiagonalLinesNegative1, tempQn.get(indexDiagonalLinesNegative1) - 1);
        tempQn.replace(indexDiagonalLinesNegative2, tempQn.get(indexDiagonalLinesNegative2) - 1);

        // calculate qn when put in queen
        tempQn.replace(indexDiagonalLinesNegativeNew1, tempQn.get(indexDiagonalLinesNegativeNew1) + 1);
        tempQn.replace(indexDiagonalLinesNegativeNew2, tempQn.get(indexDiagonalLinesNegativeNew2) + 1);

        // calculate qp when put up queen
        tempQp.replace(indexDiagonalLinesPositive1, tempQp.get(indexDiagonalLinesPositive1) - 1);
        tempQp.replace(indexDiagonalLinesPositive2, tempQp.get(indexDiagonalLinesPositive2) - 1);

        // calculate qp when put in queen
        tempQp.replace(indexDiagonalLinesPositiveNew1, tempQp.get(indexDiagonalLinesPositiveNew1) + 1);
        tempQp.replace(indexDiagonalLinesPositiveNew2, tempQp.get(indexDiagonalLinesPositiveNew2) + 1);

        newCollision = tempQn.values().stream().mapToInt(this::getCollisionOnDiagonal).sum() + tempQp.values().stream().mapToInt(this::getCollisionOnDiagonal).sum();

        return newCollision - oldCollision;
    }

    public void swapQueen(int indexRow1, int indexRow2) {
        // queen 1 old column
        int indexDiagonalLinesNegative1 = computeIndexDiagonalLinesNegative(indexRow1, this.coordinatesChessBoard[indexRow1]);
        int indexDiagonalLinesPositive1 = computeIndexDiagonalLinesPositive(indexRow1, this.coordinatesChessBoard[indexRow1]);
        queenOnNegativeLine[indexDiagonalLinesNegative1] = queenOnNegativeLine[indexDiagonalLinesNegative1] - 1;
        queenOnPositiveLine[indexDiagonalLinesPositive1] = queenOnPositiveLine[indexDiagonalLinesPositive1] - 1;

        // queen 2 old column
        int indexDiagonalLinesNegative2 = computeIndexDiagonalLinesNegative(indexRow2, this.coordinatesChessBoard[indexRow2]);
        int indexDiagonalLinesPositive2 = computeIndexDiagonalLinesPositive(indexRow2, this.coordinatesChessBoard[indexRow2]);
        queenOnNegativeLine[indexDiagonalLinesNegative2] = queenOnNegativeLine[indexDiagonalLinesNegative2] - 1;
        queenOnPositiveLine[indexDiagonalLinesPositive2] = queenOnPositiveLine[indexDiagonalLinesPositive2] - 1;

        int indexColumnTemp = this.coordinatesChessBoard[indexRow1];
        this.coordinatesChessBoard[indexRow1] = this.coordinatesChessBoard[indexRow2];
        this.coordinatesChessBoard[indexRow2] = indexColumnTemp;

        // queen 1 new column
        int indexDiagonalLinesNegativeNew1 = computeIndexDiagonalLinesNegative(indexRow1, this.coordinatesChessBoard[indexRow1]);
        int indexDiagonalLinesPositiveNew1 = computeIndexDiagonalLinesPositive(indexRow1, this.coordinatesChessBoard[indexRow1]);
        queenOnNegativeLine[indexDiagonalLinesNegativeNew1] = queenOnNegativeLine[indexDiagonalLinesNegativeNew1] + 1;
        queenOnPositiveLine[indexDiagonalLinesPositiveNew1] = queenOnPositiveLine[indexDiagonalLinesPositiveNew1] + 1;

        // queen 2 new column
        int indexDiagonalLinesNegativeNew2 = computeIndexDiagonalLinesNegative(indexRow2, this.coordinatesChessBoard[indexRow2]);
        int indexDiagonalLinesPositiveNew2 = computeIndexDiagonalLinesPositive(indexRow2, this.coordinatesChessBoard[indexRow2]);
        queenOnNegativeLine[indexDiagonalLinesNegativeNew2] = queenOnNegativeLine[indexDiagonalLinesNegativeNew2] + 1;
        queenOnPositiveLine[indexDiagonalLinesPositiveNew2] = queenOnPositiveLine[indexDiagonalLinesPositiveNew2] + 1;
    }

    public void searchQueen() {
        int collision = 1;                    // stores total number of collisions on diagonal line

        while (collision > 0) {
            attack.clear();
            shuffleArray();
            collision = countQueenOnDiagonal();
            computeAttacks();

            System.out.println("======================================");

            double limit = C1 * collision;
            long maxLoop = C2 * size * size;
            long loopCount = 0L;

            while (loopCount <= maxLoop) {
                Integer[] attackArr = new Integer[attack.size()];
                attack.toArray(attackArr);

                for (int i = 0; i < attackArr.length; i++) {
                    int indexRow = attackArr[i];
                    int indexRowRandom;
                    do {
                        indexRowRandom = random.nextInt(size - 1);
                    } while (indexRowRandom == indexRow);

                    int differenceCollision = computeCollisionIfSwapTemp(indexRow, indexRowRandom);
                    if (differenceCollision < 0) {
//						System.out.print("index row: " + indexRow + ", ");
//						System.out.print("index row random: " + indexRowRandom + ", ");

                        swapQueen(indexRow, indexRowRandom);
                        collision += differenceCollision;
                        System.out.println("collision: " + collision);
                        if (collision == 0) {
                            System.out.println("=========== loop count: " + loopCount + " ===========");
                            return;
                        }
                        if (collision < limit) {
                            limit = C1 * collision;
                            computeAttacks();
                            i = 0;
                        }
                    }
                }
                loopCount += attack.size();
            }
        }

        return;
    }

}
