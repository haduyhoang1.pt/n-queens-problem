package oop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of chess board: ");
		int sizeChessBoard = sc.nextInt();

		long range = System.currentTimeMillis();
		ChessBoard chessBoard = new ChessBoard(sizeChessBoard);
		chessBoard.searchQueen();
		range = System.currentTimeMillis() - range;

		System.out.println("======================= time: " + range + " =======================");

		System.out.println("======================= check =======================");
		check(chessBoard.getCoordinatesChessBoard());
		System.out.println("======================= check done =======================");
	}

	public static void check(int[] indexChessBoard) {
		List<Integer> qn = new ArrayList<>();
		List<Integer> qp = new ArrayList<>();
		// initialize
		int numberOfDiagonals = 2 * indexChessBoard.length - 1;
		for (int i = 0; i < numberOfDiagonals; i++) {
			qn.add(0);
			qp.add(0);
		}

		// check trùng trên chỉ số cột
		// queen phải không chứa các phần tử trùng nhau
		if ( Arrays.stream(indexChessBoard).distinct().count() != indexChessBoard.length) {
			System.out.println("array queen having tow element same value (column positions)");
			return;
		}
		// queen không chứa các phần tử trùng nhau và phần tử lớn nhất là n = size của mảng => queen là mảng hoán vị của 0 -> n-1
		int maxIndexColumn = Arrays.stream(indexChessBoard).distinct().max().getAsInt();
		if (maxIndexColumn != indexChessBoard.length - 1) {
			System.out.println("array Q is not continuous from 1 to N. max = " + maxIndexColumn);
			return;
		}

		// check trùng trên đường chéo
		for (int i = 0; i < indexChessBoard.length; i++) {
			// diagonal lines negative
			int indexDiagonalLinesNegative = computeIndexDiagonalLinesNegativeS(i, indexChessBoard[i]);
			if (qn.set(indexDiagonalLinesNegative, qn.get(indexDiagonalLinesNegative) + 1) > 0) {
				System.out.printf("diagonal line negative have index: %d wrong/n", indexDiagonalLinesNegative);
				return;
			}

			// diagonal lines positive
			int indexDiagonalLinesPositive = computeIndexDiagonalLinesPositiveS(i, indexChessBoard[i], indexChessBoard.length);
			if (qp.set(indexDiagonalLinesPositive, qp.get(indexDiagonalLinesPositive) + 1) > 0) {
				System.out.printf("diagonal line positive have index: %d wrong/n", indexDiagonalLinesPositive);
				return;
			}
		}
	}

	public static int computeIndexDiagonalLinesNegativeS(int row, int column) {
		return row + column;
	}

	/**
	 * @param row
	 * @param column
	 * @return index of diagonal line positive
	 */
	public static int computeIndexDiagonalLinesPositiveS(int row, int column, int sizeOfChessBoard) {
		return row - column + (sizeOfChessBoard - 1);
	}

}
